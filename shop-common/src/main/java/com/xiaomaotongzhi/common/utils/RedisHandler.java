package com.xiaomaotongzhi.common.utils;

import cn.hutool.json.JSONUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.function.Function;

import static com.xiaomaotongzhi.common.utils.Constants.*;

@Component
public class RedisHandler implements InitializingBean {
    @Autowired
    private StringRedisTemplate redisTemplate ;

    private Cache<String, String> cache ;

    public void setCache(String key ,String value ) {
        try {
            cache.put(key,value);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getCache(String key ) {
        try {
            return cache.getIfPresent(key);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveString(String key ,String value) {
        redisTemplate.opsForValue().set(key ,value); ;
        setCache(key, value);
    }

    public void saveString(String key ,String value ,Integer timeout) {
        redisTemplate.opsForValue().set(key ,value ,timeout) ;
        setCache(key, value);
    }

    public String getString(String key) {
        if (getCache(key) == null) {
            return redisTemplate.opsForValue().get(key);
        }
        return null ;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        cache = Caffeine.newBuilder().maximumSize(CACHE_MAX_SIZE)
                .expireAfterWrite(Duration.ofMinutes(CACHE_EXPIRE_TIME))
                .build();
    }
}
