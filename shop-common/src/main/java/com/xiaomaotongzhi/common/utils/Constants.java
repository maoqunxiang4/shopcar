package com.xiaomaotongzhi.common.utils;

public class Constants {
    public static final Integer CACHE_MAX_SIZE = 1000000 ;
    public static final Integer CACHE_EXPIRE_TIME = 10 ;
    public static final String USER_INFO = "user:info:" ;
    public static final String VEHICLE_PRODUCTS_ID = "vehicle:products:id:" ;
//    public static final String VEHICLE_PRODUCTS_ID = "vehicle:products:" ;
    public static final String VEHICLE_PRODUCTS_INFO = "vehicle:products:info:" ;
    public static final String VEHICLE_PRODUCTS_EARNING = "vehicle:products:earnings" ;
    public static final String DEFAUT_PIC_URL = "" ;
    public static final String PRODUCT_INFO = "product:info:" ;
    public static final String REPLENISHMENT_STATION_PRODUCTS = "replenishment:station:products:" ;
    public static final String REPLENISHMENT_STATION_PRODUCT_ID = "replenishment:station:product:id:" ;
    public static final String REPLENISHMENT_STATION_REQUEST_ID = "replenishment:station:request:id:" ;
    public static final String REPLENISHMENT_STATION_EARNING = "replenishment:station:earnings" ;

}
