package com.xiaomaotongzhi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@MapperScan("com.xiaomaotongzhi.mapper")
@EnableSwagger2WebMvc
public class ShopCarApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopCarApplication.class, args);
    }
}