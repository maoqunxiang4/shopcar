package com.xiaomaotongzhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.domain.po.Addition.User;


public interface IUserService extends IService<User> {

    // 用户登录
    Result login(String username, String password);

    // 历史订单查询
    Result getHistoricalOrders();

    // 等级查询
    Result getUserLevel();

    // 完善个人信息
    Result refineInfo(String nickName ,String realName ,String userMobile ,String sex ,String birthDate) ;

    // 注册司机
    Result registerDriver(String license ,String location );

    // 注册供货站
    Result registerStation(String store_name, String contact_info,
                           String address_detils, String station_type ) ;
}

