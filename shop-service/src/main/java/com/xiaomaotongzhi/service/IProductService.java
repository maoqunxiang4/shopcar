package com.xiaomaotongzhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.domain.po.Addition.Order;
import com.xiaomaotongzhi.domain.po.Addition.Product;

import java.util.List;

public interface IProductService extends IService<Product>  {

    // 查看商品介绍
    Result getProductInfo(Long driverId , Long productId);

    // 商品购买
    Result purchaseProduct(Long productId, int quantity ,double sum ,Long driverId);

    // 扫码查看商品信息
    Result scanVehicleProducts(Long vehicleId);
}
