package com.xiaomaotongzhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.xiaomaotongzhi.domain.po.ReplenishmentRequest;
import com.xiaomaotongzhi.domain.po.ReplenishmentStation;

import java.math.BigDecimal;
import java.util.List;

public interface IReplenishmentStationService extends IService<ReplenishmentStation> {

    // 查看、接收申请
    Result getReplenishmentRequests();
    Result respondToReplenishmentRequest(Long requestId);
    // 增删改查商品信息
    Result getProducts();
    Result addProductInfo(Long productId);
    Result productInc(Long productId);
    Result productDec(Long productId);
    Result productStoreChange(Long productId ,Integer num);
    Result deleteProduct(Long productId);
    // 计算收益
    Result calculateEarnings();
}
