package com.xiaomaotongzhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaotongzhi.domain.po.Machine;

public interface IMachineService extends IService<Machine> {
}
