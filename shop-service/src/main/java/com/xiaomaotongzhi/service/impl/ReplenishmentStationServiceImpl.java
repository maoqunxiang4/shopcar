package com.xiaomaotongzhi.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.dto.RequestProductDTO;
import com.xiaomaotongzhi.domain.dto.UserInfoDTO;
import com.xiaomaotongzhi.domain.po.Addition.User;
import com.xiaomaotongzhi.domain.po.ReplenishmentRequest;
import com.xiaomaotongzhi.domain.po.ReplenishmentStation;
import com.xiaomaotongzhi.domain.po.Vehicle;
import com.xiaomaotongzhi.mapper.*;
import com.xiaomaotongzhi.service.IReplenishmentStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.xiaomaotongzhi.common.utils.Constants.*;

@Service
public class ReplenishmentStationServiceImpl extends ServiceImpl<ReplenishmentStationMapper , ReplenishmentStation> implements IReplenishmentStationService {
    @Autowired
    private StringRedisTemplate redisTemplate ;

    @Autowired
    private ProductMapper productMapper ;

    @Autowired
    private ReplenishmentRequestMapper replenishmentRequestMapper ;

    @Autowired
    private UserMapper userMapper ;

    @Autowired
    private VehicleMapper vehicleMapper ;


    @Override
    public Result getReplenishmentRequests() {
        Set<String> members = redisTemplate.opsForSet().members(REPLENISHMENT_STATION_REQUEST_ID + getUserInfoDTO().getStationId());
        List<RequestProductDTO> requestProductDTOList = new ArrayList<>() ;
        if (CollUtil.isEmpty(members)) return Result.success(200 ,requestProductDTOList) ;
        List<ReplenishmentRequest> replenishmentRequests = replenishmentRequestMapper.selectBatchIds(members);
        replenishmentRequests.forEach(replenishmentRequest -> {
            Vehicle vehicle = vehicleMapper.selectById(replenishmentRequest.getVehicleId());
            User user = userMapper.selectById(vehicle.getOwnerId());
            requestProductDTOList.add(new RequestProductDTO(user.getRealName() ,user.getUserMobile()
                    , replenishmentRequest.getReplenishmentList())) ;
        } );

        return Result.success(200 ,requestProductDTOList);
    }

    @Override
    public Result respondToReplenishmentRequest(Long requestId) {
        if (Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(REPLENISHMENT_STATION_REQUEST_ID + getUserInfoDTO().getStationId()
                , requestId.toString()))) {
            redisTemplate.opsForSet().remove(REPLENISHMENT_STATION_REQUEST_ID + getUserInfoDTO().getStationId()
                    , requestId.toString()) ;
            ReplenishmentRequest replenishmentRequest = replenishmentRequestMapper.selectById(requestId);
            replenishmentRequest.setStatus("已完成");
            replenishmentRequestMapper.updateById(replenishmentRequest) ;
            return Result.success(200) ;
        }
        throw new RuntimeException("Error request, this request is not supported") ;
    }

    @Override
    public Result getProducts() {
        Set<String> members = redisTemplate.opsForSet().members(REPLENISHMENT_STATION_PRODUCTS + getUserInfoDTO().getStationId());
        if (!CollUtil.isEmpty(members)) return Result.success(200 ,productMapper.selectBatchIds(members)) ;
        return Result.success(200 ,new HashSet<String>()) ;
    }

    @Override
    public Result addProductInfo(Long productId) {
        redisTemplate.opsForHash().put(REPLENISHMENT_STATION_PRODUCTS + getUserInfoDTO().getStationId()  , productId.toString(), String.valueOf(1));
        redisTemplate.opsForSet().add(REPLENISHMENT_STATION_PRODUCT_ID + getUserInfoDTO().getStationId() , productId.toString()) ;
        return Result.success(200);
    }

    @Override
    public Result productInc(Long productId) {
        redisTemplate.opsForHash().increment(REPLENISHMENT_STATION_PRODUCTS + getUserInfoDTO().getStationId() , productId.toString() , 1) ;
        return Result.success(200);
    }

    @Override
    public Result productDec(Long productId) {
        Object o = redisTemplate.opsForHash().get(REPLENISHMENT_STATION_PRODUCTS + getUserInfoDTO().getStationId(), productId.toString());
        if(Objects.isNull(o)) {
            throw new RuntimeException("product is null") ;
        } else if (Integer.parseInt(o.toString()) == 0) {
            throw new RuntimeException("product has no store") ;
        } else {
            redisTemplate.opsForHash().increment(REPLENISHMENT_STATION_PRODUCTS +
                    getUserInfoDTO().getStationId() , productId.toString() , -1) ;
        }
        return Result.success(200);
    }

    @Override
    public Result productStoreChange(Long productId ,Integer num) {
        redisTemplate.opsForHash().put(REPLENISHMENT_STATION_PRODUCTS + getUserInfoDTO().getStationId() , productId.toString() , num.toString()) ;
        return Result.success(200);
    }

    @Override
    public Result deleteProduct(Long productId) {
        //直接从redis中删除
        redisTemplate.opsForSet().remove(REPLENISHMENT_STATION_PRODUCT_ID + getUserInfoDTO().getStationId() ,
                productId.toString()) ;
        return Result.success(200) ;
    }

    @Override
    public Result calculateEarnings() {
        Object o = redisTemplate.opsForHash()
                .get(REPLENISHMENT_STATION_EARNING, getUserInfoDTO().getStationId().toString());
        if (Objects.isNull(o)) {
            return  Result.success(200,o);
        }
        return Result.fail(400 ,"error calculating") ;
    }

    public UserInfoDTO getUserInfoDTO() {
        return JSONUtil.toBean(redisTemplate.opsForValue()
                .get(USER_INFO + UserContext.getUserId()), UserInfoDTO.class);
    }

}
