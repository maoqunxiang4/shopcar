package com.xiaomaotongzhi.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.dto.UserInfoDTO;
import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.xiaomaotongzhi.domain.po.ReplenishmentRequest;
import com.xiaomaotongzhi.domain.po.Vehicle;
import com.xiaomaotongzhi.mapper.ProductMapper;
import com.xiaomaotongzhi.mapper.ReplenishmentRequestMapper;
import com.xiaomaotongzhi.mapper.UserMapper;
import com.xiaomaotongzhi.mapper.VehicleMapper;
import com.xiaomaotongzhi.service.IReplenishmentStationService;
import com.xiaomaotongzhi.service.IVehicleProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.xiaomaotongzhi.common.utils.Constants.*;

@Service
public class VehicleProductProductServiceImpl extends ServiceImpl<VehicleMapper ,Vehicle> implements IVehicleProductService {

    @Autowired
    private StringRedisTemplate redisTemplate ;

    @Autowired
    private ProductMapper productMapper ;

    @Autowired
    private ReplenishmentRequestMapper replenishmentRequestMapper ;

    @Override
    public Result getProducts() {
        Set<String> members = redisTemplate.opsForSet().members(VEHICLE_PRODUCTS_ID + getUserInfoDTO().getVehicleId());
        if (CollUtil.isNotEmpty(members)) return Result.success(200 ,productMapper.selectBatchIds(members)) ;
        return Result.success(200,new HashSet<String>()) ;
    }

    @Override
    public Result addProductInfo(Long productId) {
        redisTemplate.opsForHash().put(VEHICLE_PRODUCTS_INFO + getUserInfoDTO().getVehicleId()  , productId.toString() , String.valueOf(1));
        redisTemplate.opsForSet().add(VEHICLE_PRODUCTS_ID + getUserInfoDTO().getVehicleId(), productId.toString()) ;
        return Result.success(200);
    }

    @Override
    public Result productInc(Long productId) {
        redisTemplate.opsForHash().increment(VEHICLE_PRODUCTS_INFO + getUserInfoDTO().getVehicleId() , productId.toString() , 1) ;
        return Result.success(200);
    }

    @Override
    public Result productDec(Long productId) {
        String vehicleProductsKey = VEHICLE_PRODUCTS_INFO + getUserInfoDTO().getVehicleId();
        // 从 Redis 中获取产品库存
        String proStoreStr = (String) redisTemplate.opsForHash().get(vehicleProductsKey, productId.toString());
        if (proStoreStr == null) {
            throw new RuntimeException("商品不可查询");
        }
        int proStore = Integer.parseInt(proStoreStr);
        if (proStore == 0) {
            throw new RuntimeException("商品库存为0，不可删除");
        } else {
            // 减少库存
            redisTemplate.opsForHash().put(vehicleProductsKey, productId.toString(), String.valueOf(proStore - 1));
        }
        return Result.success(200);
    }


    @Override
    public Result productStoreChange(Long productId ,Integer num) {
        if (num < 0 ) throw new RuntimeException("num must be greater than 0") ;
        redisTemplate.opsForHash().put(VEHICLE_PRODUCTS_INFO + getUserInfoDTO().getVehicleId() , productId.toString() , num.toString()) ;
        return Result.success(200);
    }

    @Override
    public Result deleteProduct(Long productId) {
        //直接从redis中删除
        redisTemplate.opsForSet().remove(VEHICLE_PRODUCTS_ID + getUserInfoDTO().getVehicleId() ,
                productId.toString()) ;
        return Result.success(200) ;
    }

    @Override
    public Result calculateEarnings() {
        Optional<Object> earningObj = Optional.ofNullable(redisTemplate.opsForHash().get(VEHICLE_PRODUCTS_EARNING, getUserInfoDTO().getVehicleId().toString()));
        Double earning = earningObj.map(obj -> Double.valueOf(obj.toString())).orElse(null);
        if (earningObj.isEmpty()) {
            redisTemplate.opsForHash().put(VEHICLE_PRODUCTS_EARNING, getUserInfoDTO().getVehicleId().toString(), String.valueOf(0));
            throw new RuntimeException("error");
        } else {
            return Result.success(200, earning);
        }
    }

    @Override
    public Result sendReplenishmentRequests(Long stationId, String replenishmentListJSON) {
        ReplenishmentRequest replenishmentRequest = new ReplenishmentRequest();
        replenishmentRequest.setReplenishmentList(replenishmentListJSON) ;
        replenishmentRequest.setVehicleId(getUserInfoDTO().getVehicleId()) ;
        replenishmentRequest.setStatus("未完成") ;
        replenishmentRequestMapper.insert(replenishmentRequest) ;
        redisTemplate.opsForSet().add(REPLENISHMENT_STATION_REQUEST_ID + stationId
                                      , replenishmentRequest.getRequestId().toString() ) ;
         return Result.success(200);
    }

    public UserInfoDTO getUserInfoDTO() {
        return JSONUtil.toBean(redisTemplate.opsForValue()
                .get(USER_INFO + UserContext.getUserId()), UserInfoDTO.class);
    }
}
