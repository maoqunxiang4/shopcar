package com.xiaomaotongzhi.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.common.utils.RedisHandler;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.dto.UserInfoDTO;
import com.xiaomaotongzhi.domain.po.Addition.Order;
import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.xiaomaotongzhi.domain.po.Addition.User;
import com.xiaomaotongzhi.domain.po.Vehicle;
import com.xiaomaotongzhi.domain.po.builder.Direct;
import com.xiaomaotongzhi.domain.po.builder.OrderBuilder;
import com.xiaomaotongzhi.mapper.OrderMapper;
import com.xiaomaotongzhi.mapper.ProductMapper;
import com.xiaomaotongzhi.service.IProductService;
import com.xiaomaotongzhi.service.IUserService;
import com.xiaomaotongzhi.service.IVehicleProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

import static com.xiaomaotongzhi.common.utils.Constants.*;

@Component
public class ProductServiceImpl extends ServiceImpl<ProductMapper,Product> implements IProductService {

    @Autowired
    private RedisHandler redisHandler ;

    @Autowired
    private StringRedisTemplate redisTemplate ;

    @Autowired
    private OrderMapper orderMapper ;

    @Autowired
    private Direct direct ;

    @Autowired
    private IUserService userService ;

    @Autowired
    private ProductMapper productMapper ;

    @Autowired
    private IVehicleProductService vehicleProductService ;


    @Override
    public Result getProductInfo(Long driverId, Long productId) {
        //检查
        Vehicle vehicle = vehicleProductService.query().eq("owner_id", driverId.toString()).one();
        if(Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(VEHICLE_PRODUCTS_ID + vehicle.getVehicleId().toString()
                , productId.toString()))) {
            String productJSON = redisHandler.getCache(PRODUCT_INFO + driverId);
            if(StringUtils.hasText(productJSON)) {
                return Result.success(200,JSONUtil.toBean(productJSON , Product.class)) ;
            } else {
                Product product = query().eq("id", productId).one();
                redisHandler.saveString(PRODUCT_INFO + productId , JSONUtil.toJsonStr(product) ,30);
                return Result.success(200 ,product) ;
            }
        }
        return Result.fail(400,"error getting") ;
    }

    @Override
    @Transactional
    public Result purchaseProduct( Long productId, int quantity ,double sum ,Long driverId) {
        int store = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForHash().get(VEHICLE_PRODUCTS_INFO + getUserInfoDTO().getVehicleId()
                , productId.toString())).toString());
        if (store - quantity < 0) throw new RuntimeException("quantity is negative") ;
        vehicleProductService.productStoreChange(productId , store - quantity) ;
        Vehicle vehicle = vehicleProductService.query().eq("owner_id", driverId).one();
        addEarnings(vehicle.getVehicleId().toString() ,sum);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("productId" ,productId) ;
        map.put("totalAmount" ,sum ) ;
        map.put("sum" , quantity) ;
        Order order = (Order) direct.directAddition(new OrderBuilder(map));
        orderMapper.insert(order) ;
        return Result.success(200,order);
    }

    @Override
    public Result scanVehicleProducts(Long vehicleId) {
        Set<String> idSet = redisTemplate.opsForSet().members(VEHICLE_PRODUCTS_ID + vehicleId);
        if ( !Objects.isNull(idSet) && !CollUtil.isEmpty(idSet)) {
            List<Product> products = productMapper.selectBatchIds(idSet);
            return Result.success(200 ,products) ;
        }
        return Result.fail(400,"error setting");
    }

    public UserInfoDTO getUserInfoDTO() {
        return JSONUtil.toBean(redisTemplate.opsForValue()
                .get(USER_INFO + UserContext.getUserId()), UserInfoDTO.class);
    }

    public void addEarnings(String vehicleId ,double earnings) {
        Optional<Object> earningObj = Optional.ofNullable(redisTemplate.opsForHash()
                .get(VEHICLE_PRODUCTS_EARNING, vehicleId));
        if (earningObj.isPresent()) {
            redisTemplate.opsForHash().increment(VEHICLE_PRODUCTS_EARNING, getUserInfoDTO().getVehicleId().toString(), earnings);
        }
    }
}
