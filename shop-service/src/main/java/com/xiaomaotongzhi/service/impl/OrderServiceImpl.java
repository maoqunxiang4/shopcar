package com.xiaomaotongzhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.domain.po.Addition.Order;
import com.xiaomaotongzhi.mapper.OrderMapper;
import com.xiaomaotongzhi.service.IOrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper , Order> implements IOrderService {
}
