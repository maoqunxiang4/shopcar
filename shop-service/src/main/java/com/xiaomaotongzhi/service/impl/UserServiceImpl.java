package com.xiaomaotongzhi.service.impl;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.common.utils.RedisHandler;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.dto.UserDTO;
import com.xiaomaotongzhi.domain.po.Addition.User;
import com.xiaomaotongzhi.domain.po.Machine;
import com.xiaomaotongzhi.domain.po.ReplenishmentStation;
import com.xiaomaotongzhi.domain.po.Vehicle;
import com.xiaomaotongzhi.domain.po.builder.Direct;
import com.xiaomaotongzhi.domain.po.builder.UserBuilder;
import com.xiaomaotongzhi.enums.MachineStatus;
import com.xiaomaotongzhi.enums.StationStatus;
import com.xiaomaotongzhi.mapper.*;
import com.xiaomaotongzhi.service.*;
import com.xiaomaotongzhi.utils.LocationUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static com.xiaomaotongzhi.common.utils.Constants.REPLENISHMENT_STATION_EARNING;
import static com.xiaomaotongzhi.common.utils.Constants.VEHICLE_PRODUCTS_EARNING;
import static com.xiaomaotongzhi.utils.LocationUtils.getCurrentLocation;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper , User> implements IUserService {
    @Autowired
    private final RedisHandler redisHandler;

    @Autowired
    private IOrderService orderService ;

    @Autowired
    private IReplenishmentStationService replenishmentStationService ;

    @Autowired
    private IMachineService machineService ;

    @Autowired
    private IVehicleProductService vehicleService ;

    @Autowired
    private Direct direct ;

    @Value("${wx.appid}")
    private String appid ;

    @Value("${wx.secret}")
    private String appseret ;

    @Autowired
    private StringRedisTemplate redisTemplate ;

    @Override
    public Result login(String js_code, String grant_type) {
        String url = "https://api.weixin.qq.com/sns/jscode2session?" +
                "appid="+ appid +"&secret=" + appseret + "&js_code=" +
                js_code + "&grant_type= " + grant_type ;
        UserDTO userDTO = JSONUtil.toBean(HttpUtil.get(url), UserDTO.class);

        //是否已经注册注册
        User user = this.query().eq("user_id", userDTO.getUnionId()).one();
        if(Objects.isNull(user)) return Result.success(200,userDTO);
        //重新注册
        this.save((User) direct.directAddition(new UserBuilder(new HashMap<>()))) ;
        return Result.success(200 ,userDTO) ;
    }


    @Override
    public Result getHistoricalOrders() {
        return Result.success(200 , orderService.query()
                .eq("user_id",UserContext.getUserId()).list() );
    }

    @Override
    public Result getUserLevel() {
        if(redisHandler.getCache(UserContext.getUserId()) == null) {
            return Result.success(200 ,query().eq("user_id", UserContext.getUserId()).one().getLevel()) ;
        }
        return Result.fail(400, "error");
    }

    @Override
    public Result refineInfo(String nickName ,String realName ,String userMobile ,String sex ,String birthDate) {
        User user = query().eq("user_id", UserContext.getUserId()).one();
        user.setNickName(nickName);
        user.setRealName(realName) ;
        user.setUserMobile(userMobile) ;
        user.setBirthDate(birthDate) ;
        user.setSex(sex) ;
        this.update().eq("user_id",UserContext.getUserId()).update(user);
        return Result.success(200);
    }

    @Override
    @Transactional
    public Result registerDriver(String license ,String location ) {
        Vehicle ve = vehicleService.query().eq("license", license).one();
        if (ve != null) {
            return Result.fail(400 ,"该牌照已经存在，请重新注册") ;
        }
        ve = vehicleService.query().eq("owner_id", UserContext.getUserId()).one();
        if (ve != null) {
            return Result.fail(400, "您已在该平台注册过了，请勿重复注册");
        }
        int count = machineService.count(new QueryWrapper<>());
        Machine machine = new Machine();
        machine.setStatus(MachineStatus.IN_USER.getStatus()) ;
        machine.setLocation(location) ;
        machine.setMachineId(count+1) ;
        machineService.save(machine) ;

        Vehicle vehicle = new Vehicle();
        vehicle.setLicense(license) ;
        vehicle.setMachineId(count+1) ;
        vehicle.setOwnerId(UserContext.getUserId()) ;
        vehicleService.save(vehicle);
        redisTemplate.opsForHash().put(VEHICLE_PRODUCTS_EARNING ,vehicle.getVehicleId().toString() ,String.valueOf(0) );
        return Result.success(200);
    }

    @Override
    public Result registerStation(String store_name, String contact_info,
                                  String address_detils, String station_type ) {
        ReplenishmentStation replenishmentStation = ReplenishmentStation.builder().storeName(store_name).contactInfo(contact_info)
                .addressDetails(address_detils).stationType(station_type)
                .stationStatus(StationStatus.IN_USER.getStatus()).build();
        CompletableFuture<LocationUtils.Coordinates> locationFuture = getCurrentLocation();
        locationFuture.thenAccept(coordinates -> {
            replenishmentStation.setLatitude(BigDecimal.valueOf(coordinates.getLatitude())) ;
            replenishmentStation.setLongitude(BigDecimal.valueOf(coordinates.getLongitude())) ;
        });
        locationFuture.join();
        replenishmentStation.setUser_id(UserContext.getUserId()) ;
        replenishmentStationService.save(replenishmentStation) ;
        redisTemplate.opsForHash().put(REPLENISHMENT_STATION_EARNING,
                replenishmentStation.getStationId().toString() ,  String.valueOf(0));
        return Result.success(200);
    }

}
