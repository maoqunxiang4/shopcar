package com.xiaomaotongzhi.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaotongzhi.domain.po.Machine;
import com.xiaomaotongzhi.mapper.MachineMapper;
import com.xiaomaotongzhi.service.IMachineService;
import org.springframework.stereotype.Service;

@Service
public class MachineServiceImpl extends ServiceImpl<MachineMapper , Machine> implements IMachineService {
}
