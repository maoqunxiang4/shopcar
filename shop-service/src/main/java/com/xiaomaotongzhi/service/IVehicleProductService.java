package com.xiaomaotongzhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.xiaomaotongzhi.domain.po.Vehicle;

public interface IVehicleProductService extends IService<Vehicle> {
    // 增删改查商品信息
    Result getProducts();
    Result addProductInfo(Long productId);
    Result productInc(Long productId);
    Result productDec(Long productId);
    Result productStoreChange(Long productId ,Integer num);
    Result deleteProduct(Long productId);
    // 计算收益
    Result calculateEarnings();
    Result sendReplenishmentRequests(Long station ,String replenishmentListJSON);
}
