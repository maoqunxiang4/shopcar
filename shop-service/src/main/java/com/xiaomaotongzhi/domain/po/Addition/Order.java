package com.xiaomaotongzhi.domain.po.Addition;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.po.builder.Builder;
import com.xiaomaotongzhi.utils.LocationUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import static com.xiaomaotongzhi.utils.LocationUtils.getCurrentLocation;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_info")
@ApiModel(value="OrderInfo对象", description="订单信息表")
public class Order extends Addition implements Serializable  {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单ID")
    @TableId(value = "order_id", type = IdType.AUTO)
    private Integer orderId;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "商品数量")
    private Integer sum;

    @ApiModelProperty(value = "纬度信息")
    private BigDecimal latitude;

    @ApiModelProperty(value = "经度信息")
    private BigDecimal longitude;
}
