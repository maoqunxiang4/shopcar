package com.xiaomaotongzhi.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 补货申请表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("replenishment_request")
@ApiModel(value="ReplenishmentRequest对象", description="补货申请表")
public class ReplenishmentRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "申请ID")
    @TableId(value = "request_id", type = IdType.AUTO)
    private Integer requestId;

    @ApiModelProperty(value = "车辆ID")
    private Integer vehicleId;

    @ApiModelProperty(value = "补货清单")
    private String replenishmentList;

    @ApiModelProperty(value = "申请状态")
    private String status;


}
