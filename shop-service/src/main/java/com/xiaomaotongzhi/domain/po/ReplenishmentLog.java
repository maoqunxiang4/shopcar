package com.xiaomaotongzhi.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 补货日志表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("replenishment_log")
@ApiModel(value="ReplenishmentLog对象", description="补货日志表")
public class ReplenishmentLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日志ID")
    @TableId(value = "log_id", type = IdType.AUTO)
    private Integer logId;

    @ApiModelProperty(value = "申请ID")
    private Integer requestId;

    @ApiModelProperty(value = "商品图片路径")
    private String imageUrl;

    @ApiModelProperty(value = "补货状态")
    private String status;


}
