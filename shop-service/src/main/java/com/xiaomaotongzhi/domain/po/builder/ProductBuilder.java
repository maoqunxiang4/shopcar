package com.xiaomaotongzhi.domain.po.builder;

import com.xiaomaotongzhi.domain.po.Addition.Addition;
import com.xiaomaotongzhi.domain.po.Addition.Product;

import java.math.BigDecimal;
import java.util.HashMap;

public class ProductBuilder extends Builder {
    Product product = new Product() ;

    public ProductBuilder(HashMap<Object, Object> map) {
        super(map);
    }

    @Override
    public Builder injectInvariant() {
        return this;
    }

    @Override
    public Builder injectChange() {
        product.setName((String) map.get("name"));
        product.setDescInfo((String) map.get("descInfo"));
        product.setPrice((BigDecimal) map.get("price"));
        product.setImgUrl((String) map.get("imgUrl"));
        product.setType((String) map.get("type"));
        return this;
    }

    @Override
    public Addition build() {
        return product;
    }

}
