package com.xiaomaotongzhi.domain.po.builder;

import com.xiaomaotongzhi.domain.po.Addition.Addition;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Objects;

@AllArgsConstructor
@Component
public class Direct {
    public Addition directAddition(Builder builder) {
        return builder.injectInvariant()
                .injectChange()
                .build() ;
    }
}
