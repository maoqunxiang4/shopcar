package com.xiaomaotongzhi.domain.po.builder;

import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.po.Addition.Addition;
import com.xiaomaotongzhi.domain.po.Addition.User;

import java.time.LocalDateTime;
import java.util.HashMap;

import static com.xiaomaotongzhi.common.utils.Constants.DEFAUT_PIC_URL;

public class UserBuilder extends Builder{

    public User user = new User() ;

    public UserBuilder(HashMap<Object, Object> map) {
        super(map);
    }

    @Override
    public Builder injectInvariant() {
        user.setId(UserContext.getUserId()) ;
        user.setModifyTime(LocalDateTime.now());
        user.setUserRegtime(LocalDateTime.now()) ;
        user.setPic(DEFAUT_PIC_URL) ;
        user.setStatus (0);
        user.setLevel(0) ;
        return this;
    }

    @Override
    public Builder injectChange() {
        user.setNickName((String) map.get("nickName"));
        user.setRealName((String) map.get("realName"));
        user.setUserMobile((String) map.get("userMobile"));
        user.setSex((String) map.get("sex"));
        user.setBirthDate((String) map.get("birthDate"));
        return this;
    }

    @Override
    public Addition build() {
        return user;
    }
}
