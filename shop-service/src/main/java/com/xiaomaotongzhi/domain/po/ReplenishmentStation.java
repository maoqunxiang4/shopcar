package com.xiaomaotongzhi.domain.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.concurrent.CompletableFuture;

import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.utils.LocationUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.InitializingBean;

import static com.xiaomaotongzhi.utils.LocationUtils.getCurrentLocation;

/**
 * <p>
 * 补货站信息表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("replenishment_station")
@Builder
@ApiModel(value="ReplenishmentStation对象", description="补货站信息表")
public class ReplenishmentStation implements Serializable , InitializingBean {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "站点ID")
    @TableId(value = "station_id", type = IdType.AUTO)
    private Integer stationId;

    @ApiModelProperty(value = "商店名称")
    private String storeName;

    @ApiModelProperty(value = "联系方式")
    private String contactInfo;

    @ApiModelProperty(value = "详细地址信息")
    private String addressDetails;

    @ApiModelProperty(value = "补货站类型")
    private String stationType;

    @ApiModelProperty(value = "站点状态")
    private String stationStatus;

    @ApiModelProperty(value = "站点纬度信息")
    private BigDecimal latitude;

    @ApiModelProperty(value = "站点经度信息")
    private BigDecimal longitude;

    @ApiModelProperty(value = "站点所属人")
    private String user_id;


    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
