package com.xiaomaotongzhi.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 车辆信息表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("vehicle")
@ApiModel(value="Vehicle对象", description="车辆信息表")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "车辆ID")
    @TableId(value = "vehicle_id", type = IdType.AUTO)
    private Integer vehicleId;

    @ApiModelProperty(value = "车主ID")
    private String ownerId;

    @ApiModelProperty(value = "自动售货机ID")
    private Integer machineId;

    @ApiModelProperty(value = "车牌号")
    private String license;


}
