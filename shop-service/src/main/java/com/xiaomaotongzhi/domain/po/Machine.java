package com.xiaomaotongzhi.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 自动售货机信息表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("machine")
@ApiModel(value="Machine对象", description="自动售货机信息表")
public class Machine implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自动售货机ID")
    @TableId(value = "machine_id")
    private Integer machineId;

    @ApiModelProperty(value = "自动售货机位置")
    private String location;

    @ApiModelProperty(value = "自动售货机状态")
    private int status;


}
