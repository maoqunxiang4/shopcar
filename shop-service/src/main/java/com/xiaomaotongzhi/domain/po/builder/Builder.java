package com.xiaomaotongzhi.domain.po.builder;

import com.xiaomaotongzhi.domain.po.Addition.Addition;

import java.util.HashMap;
import java.util.Objects;

public abstract class Builder {

    HashMap<Object ,Object> map ;

    public Builder(HashMap<Object, Object> map) {
        this.map = map;
    }

    public abstract Builder injectInvariant () ;
    public abstract Builder injectChange() ;
    public abstract Addition build () ;
}

