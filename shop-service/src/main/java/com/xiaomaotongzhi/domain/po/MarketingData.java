package com.xiaomaotongzhi.domain.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 营销数据表
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("marketing_data")
@ApiModel(value="MarketingData对象", description="营销数据表")
public class MarketingData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    @TableId(value = "data_id", type = IdType.AUTO)
    private Integer dataId;

    @ApiModelProperty(value = "车辆ID")
    private Integer vehicleId;

    @ApiModelProperty(value = "销售数据")
    private BigDecimal salesData;


}
