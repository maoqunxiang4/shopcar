package com.xiaomaotongzhi.domain.po.builder;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.po.Addition.Addition;
import com.xiaomaotongzhi.domain.po.Addition.Order;
import com.xiaomaotongzhi.utils.LocationUtils;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

import static com.xiaomaotongzhi.utils.LocationUtils.getCurrentLocation;

public class OrderBuilder extends Builder {

    Order order = new Order() ;

    public OrderBuilder(HashMap<Object, Object> map) {
        super(map);
    }

    @Override
    public Builder injectInvariant() {
        order.setUserId(UserContext.getUserId()) ;
        CompletableFuture<LocationUtils.Coordinates> locationFuture = getCurrentLocation();
        locationFuture.thenAccept(coordinates -> {
            order.setLatitude(BigDecimal.valueOf(coordinates.getLatitude())) ;
            order.setLongitude(BigDecimal.valueOf(coordinates.getLongitude())) ;
        });

        // 阻塞等待异步获取经纬度信息的完成
        locationFuture.join();
        return this;
    }

    @Override
    public Builder injectChange() {
        order.setProductId(Integer.valueOf(map.get("productId").toString()));
        order.setTotalAmount(BigDecimal.valueOf( Double.parseDouble(map.get("totalAmount").toString())));
        order.setSum(Integer.valueOf(map.get("sum").toString()));
        return this;
    }

    @Override
    public Addition build() {
        return order;
    }

}
