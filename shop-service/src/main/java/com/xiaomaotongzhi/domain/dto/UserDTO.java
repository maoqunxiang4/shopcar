package com.xiaomaotongzhi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String sessionKey;
    private String unionId;
    private String errMsg;
    private String openId;
    private int errCode;
}
