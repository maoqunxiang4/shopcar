package com.xiaomaotongzhi.domain.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoDTO {
    private String unionId;
    private Integer stationId ;
    private Integer vehicleId ;
}
