package com.xiaomaotongzhi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestProductDTO {
    private String username;
    private String phone;
    private String requestProductJSON ;
}
