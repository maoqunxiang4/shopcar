package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.ReplenishmentStation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 补货站信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface ReplenishmentStationMapper extends BaseMapper<ReplenishmentStation> {

}
