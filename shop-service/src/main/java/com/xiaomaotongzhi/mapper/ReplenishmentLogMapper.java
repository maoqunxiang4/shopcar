package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.ReplenishmentLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 补货日志表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface ReplenishmentLogMapper extends BaseMapper<ReplenishmentLog> {

}
