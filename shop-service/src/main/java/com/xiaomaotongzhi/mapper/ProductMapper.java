package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 产品信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

}
