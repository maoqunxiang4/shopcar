package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.ReplenishmentRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 补货申请表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface ReplenishmentRequestMapper extends BaseMapper<ReplenishmentRequest> {

}
