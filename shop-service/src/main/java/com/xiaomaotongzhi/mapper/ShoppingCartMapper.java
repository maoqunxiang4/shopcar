package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}
