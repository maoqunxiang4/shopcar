package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.Addition.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
