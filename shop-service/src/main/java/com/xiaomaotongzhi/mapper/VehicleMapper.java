package com.xiaomaotongzhi.mapper;

import com.xiaomaotongzhi.domain.po.Vehicle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 车辆信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-01
 */
@Mapper
public interface VehicleMapper extends BaseMapper<Vehicle> {

}
