package com.xiaomaotongzhi.intercepter;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaotongzhi.common.utils.UserContext;
import com.xiaomaotongzhi.domain.dto.UserInfoDTO;
import com.xiaomaotongzhi.domain.po.Addition.User;
import com.xiaomaotongzhi.domain.po.Machine;
import com.xiaomaotongzhi.domain.po.ReplenishmentStation;
import com.xiaomaotongzhi.domain.po.Vehicle;
import com.xiaomaotongzhi.mapper.MachineMapper;
import com.xiaomaotongzhi.mapper.ReplenishmentStationMapper;
import com.xiaomaotongzhi.mapper.UserMapper;
import com.xiaomaotongzhi.mapper.VehicleMapper;
import com.xiaomaotongzhi.utils.JwtTool;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Objects;

import static com.xiaomaotongzhi.common.utils.Constants.USER_INFO;

@RequiredArgsConstructor
public class LoginIntercepter implements HandlerInterceptor {

    private final JwtTool jwtTool ;
    private final UserMapper userMapper ;
    private final VehicleMapper vehicleMapper;
    private final ReplenishmentStationMapper replenishmentStationMapper ;
    private final StringRedisTemplate stringRedisTemplate ;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String token = request.getHeader("authentication");
//        Long parseToken = jwtTool.parseToken(token);
//        UserContext.setUserId(parseToken.toString());
        UserContext.setUserId(String.valueOf(4));
        User user = userMapper.selectById(4);
        Vehicle vehicle = vehicleMapper.selectOne(new QueryWrapper<Vehicle>().eq("owner_id", user.getId()));
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        ReplenishmentStation replenishmentStation = replenishmentStationMapper.selectOne(new QueryWrapper<ReplenishmentStation>().eq("user_id", user.getId()));
        if (!Objects.isNull(replenishmentStation)) userInfoDTO.setStationId(replenishmentStation.getStationId());
        if (!Objects.isNull(vehicle)) userInfoDTO.setVehicleId(vehicle.getVehicleId());
        userInfoDTO.setUnionId(user.getId());
        stringRedisTemplate.opsForValue().set(USER_INFO + user.getId() , JSONUtil.toJsonStr(userInfoDTO));
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        UserContext.removeUserId();
    }
}
