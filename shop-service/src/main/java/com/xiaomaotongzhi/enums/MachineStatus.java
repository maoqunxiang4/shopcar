package com.xiaomaotongzhi.enums;

public enum MachineStatus {
    IN_USER(1 ,"使用中") ,
    UN_USE(0 ,"未使用") ,
    ;

    private int status ;
    private String desc ;

    MachineStatus(int status, String desc) {
        this.status = status ;
        this.desc = desc ;
    }

    public int getStatus() {
        return status ;
    }

    public boolean of(int status) {
        if(status == 1) return true ;
        else if (status == 0) return false ;
        throw new RuntimeException("error") ;
    }

}
