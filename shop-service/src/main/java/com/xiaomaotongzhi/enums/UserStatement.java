package com.xiaomaotongzhi.enums;

import lombok.Getter;

@Getter
public enum UserStatement {
    VALID(0 ,"valid") ,
    UN_VALID(1 ,"un_valid") ,
    ;

    private final int value ;
    private final String desc ;

    UserStatement(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static UserStatement of(int value) {
        if(value == 0) {
            return VALID ;
        } else if(value == 1) {
            return UN_VALID ;
        }
        throw new RuntimeException("账号异常") ;
    }

}
