package com.xiaomaotongzhi.enums;

public enum StationStatus {
    IN_USER(1, "营业中"),
    UN_USE(0, "休息中"),
    ;

    private int status;
    private String desc;

    StationStatus(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public String  getStatus() {
        return desc;
    }

    public boolean of(int status) {
        if (status == 1) return true;
        else if (status == 0) return false;
        throw new RuntimeException("error");
    }
}
