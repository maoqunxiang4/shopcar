package com.xiaomaotongzhi.config;

import cn.hutool.core.collection.CollUtil;
import com.xiaomaotongzhi.domain.po.ReplenishmentStation;
import com.xiaomaotongzhi.intercepter.LoginIntercepter;
import com.xiaomaotongzhi.mapper.ReplenishmentStationMapper;
import com.xiaomaotongzhi.mapper.UserMapper;
import com.xiaomaotongzhi.mapper.VehicleMapper;
import com.xiaomaotongzhi.utils.JwtTool;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
@RequiredArgsConstructor
@EnableWebMvc
@EnableConfigurationProperties(AuthProperties.class)
public class MvcConfig implements WebMvcConfigurer {
    private final JwtTool jwtTool ;
    private final AuthProperties authProperties ;

    @Autowired
    private UserMapper userMapper ;
    @Autowired
    private VehicleMapper vehicleMapper;
    @Autowired
    private ReplenishmentStationMapper replenishmentStationMapper ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        WebMvcConfigurer.super.addInterceptors(registry);
        LoginIntercepter loginIntercepter = new LoginIntercepter(jwtTool,userMapper,vehicleMapper,replenishmentStationMapper,stringRedisTemplate);
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(loginIntercepter);
        List<String> includePaths = authProperties.getIncludePaths();
        if (CollUtil.isNotEmpty(includePaths)) {
            interceptorRegistration.addPathPatterns(includePaths);
        }
        // 3.配置放行路径
//        List<String> excludePaths = authProperties.getExcludePaths();
//        if (CollUtil.isNotEmpty(excludePaths)) {
//            interceptorRegistration.excludePathPatterns(excludePaths);
//        }
//        interceptorRegistration.excludePathPatterns(
//                "/**"
//        );
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new FormHttpMessageConverter());
        converters.add(new MappingJackson2HttpMessageConverter());
        // 添加其他需要的消息转换器
    }
}
