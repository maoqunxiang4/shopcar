package com.xiaomaotongzhi.controller;

import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.domain.po.Addition.Product;
import com.xiaomaotongzhi.service.IReplenishmentStationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/replenishmentStation")
@RequiredArgsConstructor
@Api(tags = "补货站管理接口")
public class ReplenishmentStationController {

    @Autowired
    private IReplenishmentStationService replenishmentStationService;

    // 查看、接收申请
    @ApiOperation("查看申请")
    @GetMapping("/replenishment-requests")
    public Result getReplenishmentRequests() {
        return replenishmentStationService.getReplenishmentRequests();
    }

    @ApiOperation("响应补货申请")
    @ApiImplicitParam(name = "requestId", value = "申请ID", required = true, dataType = "Long", paramType = "query")
    @PostMapping("/replenishment-requests/respond")
    public Result respondToReplenishmentRequest(@RequestParam Long requestId) {
        return replenishmentStationService.respondToReplenishmentRequest(requestId);
    }

    // 增删改查商品信息
    @ApiOperation("获取商品信息")
    @GetMapping("/products")
    public Result getProducts() {
        return replenishmentStationService.getProducts();
    }

    @ApiOperation("添加商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query")
    })
    @PostMapping("/products")
    public Result addProduct(@RequestParam Long productId) {
        return replenishmentStationService.addProductInfo(productId);
    }

    @ApiOperation("增加商品库存")
    @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query")
    @PutMapping("/products/inc")
    public Result productInc(@RequestParam Long productId) {
        return replenishmentStationService.productInc(productId);
    }

    @ApiOperation("减少商品库存")
    @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query")
    @PutMapping("/products/dec")
    public Result productDec(@RequestParam Long productId) {
        return replenishmentStationService.productDec(productId);
    }

    @ApiOperation("修改商品库存")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "num", value = "数量", required = true, dataType = "Integer", paramType = "query")
    })
    @PutMapping("/products/store")
    public Result productStoreChange(@RequestParam Long productId, @RequestParam Integer num) {
        return replenishmentStationService.productStoreChange(productId, num);
    }

    @ApiOperation("删除商品")
    @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query")
    @DeleteMapping("/products/{productId}")
    public Result deleteProduct(@PathVariable Long productId) {
        return replenishmentStationService.deleteProduct(productId);
    }

    // 计算收益
    @ApiOperation("计算收益")
    @GetMapping("/earnings")
    public Result calculateEarnings() {
        return replenishmentStationService.calculateEarnings();
    }

}

