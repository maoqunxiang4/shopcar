package com.xiaomaotongzhi.controller;

import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
@Api(tags = "商品管理接口")
public class ProductController {

    @Autowired
    private IProductService productService;

    // 查看商品介绍
    @ApiOperation("获取商品信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "driverId", value = "司机ID", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query")
    })
    @GetMapping("/info")
    public Result getProductInfo(@RequestParam Long driverId, @RequestParam Long productId) {
        return productService.getProductInfo(driverId, productId);
    }

    // 商品购买
    @ApiOperation("购买商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "quantity", value = "购买数量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sum", value = "总价", required = true, dataType = "double", paramType = "query") ,
            @ApiImplicitParam(name = "driverId", value = "司机ID", required = true, dataType = "Long", paramType = "query")
    })
    @PostMapping("/purchase")
    public Result purchaseProduct(@RequestParam Long productId, @RequestParam int quantity, @RequestParam double sum ,
    @RequestParam Long driverId) {
        return productService.purchaseProduct(productId, quantity, sum ,driverId);
    }

    // 扫码查看商品信息
    @ApiOperation("扫描车辆上的商品信息")
    @ApiImplicitParam(name = "vehicleId", value = "车辆ID", required = true, dataType = "Long", paramType = "query")
    @GetMapping("/scan")
    public Result scanVehicleProducts(@RequestParam Long vehicleId) {
        return productService.scanVehicleProducts(vehicleId);
    }
}

