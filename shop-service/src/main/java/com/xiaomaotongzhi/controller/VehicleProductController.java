package com.xiaomaotongzhi.controller;

import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.service.IVehicleProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vehicle")
@RequiredArgsConstructor
@Api(tags = "车载商品管理接口")
public class VehicleProductController {

    private final IVehicleProductService vehicleProductService;

    @ApiOperation("获取商品信息")
    @GetMapping("/products")
    public Result getProducts() {
        return vehicleProductService.getProducts();
    }

    @ApiOperation("添加商品信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
    })
    @PostMapping("/product")
    public Result addProductInfo(@RequestParam Long productId) {
        return vehicleProductService.addProductInfo(productId);
    }

    @ApiOperation("增加商品库存")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
    })
    @PutMapping("/product/inc/{productId}")
    public Result productInc(@PathVariable Long productId) {
        return vehicleProductService.productInc(productId);
    }

    @ApiOperation("减少商品库存")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
    })
    @PutMapping("/product/dec/{productId}")
    public Result productDec(@PathVariable Long productId) {
        return vehicleProductService.productDec(productId);
    }

    @ApiOperation("修改商品库存")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "num", value = "商品库存", required = true, dataType = "Integer", paramType = "query"),
    })
    @PutMapping("/product/change/{productId}")
    public Result productStoreChange(@PathVariable Long productId, @RequestParam Integer num) {
        return vehicleProductService.productStoreChange(productId, num);
    }

    @ApiOperation("删除商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "商品ID", required = true, dataType = "Long", paramType = "query"),
    })
    @DeleteMapping("/product/{productId}")
    public Result deleteProduct(@PathVariable Long productId) {
        return vehicleProductService.deleteProduct(productId);
    }

    @ApiOperation("计算收益")
    @GetMapping("/earnings")
    public Result calculateEarnings() {
        return vehicleProductService.calculateEarnings();
    }


    @ApiOperation("发送补货请求")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "stationId", value = "补货站ID", required = true, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "replenishmentListJSON", value = "所需商品JSON字符串（id和数量）", required = true, dataType = "String", paramType = "query"),
    })
    @PutMapping("/request/{stationId}")
    public Result sendReplenishmentRequests(@PathVariable Long stationId ,String replenishmentListJSON) {
        return vehicleProductService.sendReplenishmentRequests(stationId ,replenishmentListJSON);
    }

}

