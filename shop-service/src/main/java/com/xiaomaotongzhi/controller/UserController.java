package com.xiaomaotongzhi.controller;

import com.xiaomaotongzhi.common.utils.Result;
import com.xiaomaotongzhi.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Api(tags = "用户管理接口")
public class UserController {

    @Autowired
    private IUserService userService;

    // 用户登录
    @ApiOperation("用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "js_code", value = "code", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "grant_type", value = "grant_type", required = true, dataType = "String", paramType = "query")
    })
    @PostMapping("/login")
    public Result login(@RequestParam String js_code, @RequestParam String grant_type) {
        return userService.login(js_code, grant_type);
    }

    // 历史订单查询
    @ApiOperation("历史订单查询")
    @GetMapping("/orders/history")
    public Result getHistoricalOrders() {
        return userService.getHistoricalOrders();
    }

    // 等级查询
    @ApiOperation("等级查询")
    @GetMapping("/level")
    public Result getUserLevel() {
        return userService.getUserLevel();
    }

    // 完善个人信息
    @ApiOperation("完善个人信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "昵称", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "真实姓名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userMobile", value = "手机号码", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sex", value = "性别", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "birthDate", value = "出生日期", required = true, dataType = "String", paramType = "query")
    })
    @PostMapping("/info/refine")
    public Result refineInfo(@RequestParam String nickName, @RequestParam String realName,
                             @RequestParam String userMobile, @RequestParam String sex,
                             @RequestParam String birthDate) {
        return userService.refineInfo(nickName, realName, userMobile, sex, birthDate);
    }

    // 注册司机
    @ApiOperation("注册司机")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "license", value = "驾驶证号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "location", value = "位置", required = true, dataType = "String", paramType = "query")
    })
    @PostMapping("/driver/register")
    public Result registerDriver(@RequestParam String license, @RequestParam String location) {
        return userService.registerDriver(license, location);
    }

    // 注册供货站
    @ApiOperation("注册供货站")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "store_name", value = "商店名称", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "contact_info", value = "联系方式", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "address_detils", value = "详细地址信息", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "station_type", value = "站点类型", required = true, dataType = "String", paramType = "query")
    })
    @PostMapping("/station/register")
    public Result registerStation(@RequestParam String store_name, @RequestParam String contact_info,
                                  @RequestParam String address_detils, @RequestParam String station_type) {
        return userService.registerStation(store_name, contact_info, address_detils, station_type);
    }
}

